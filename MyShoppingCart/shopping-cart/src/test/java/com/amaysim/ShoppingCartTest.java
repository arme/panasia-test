package com.amaysim;

import com.amaysim.bean.ShoppingCart;
import com.amaysim.entity.Item;
import com.amaysim.entity.PriceRule;
import com.amaysim.entity.Product;
import com.amaysim.exception.AppException;
import com.amaysim.service.ProductService;
import com.amaysim.service.Services;
import com.amaysim.service.ShoppingCartService;
import com.amaysim.util.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by win7 on 1/25/2017.
 */
public class ShoppingCartTest {

    @Mock
    ProductService productService;

    private ShoppingCartService shoppingCartService = Services.ShoppingCartService();
    private List<PriceRule> priceRules = new ArrayList();

    @Before
    public void setup() throws AppException{
        productService = mock(ProductService.class);

        when(productService.findByCode("ult_small")).thenReturn(new Product(1, "ult_small", "Unlimited 1GB", 24.90));
        when(productService.findByCode("ult_medium")).thenReturn(new Product(2, "ult_medium", "Unlimited 2GB", 29.90));
        when(productService.findByCode("ult_large")).thenReturn(new Product(3, "ult_large", "Unlimited 5GB", 44.90));
        when(productService.findByCode("1gb")).thenReturn(new Product(4, "1gb", "1 GBData-pack", 9.90));

        priceRules.add(new PriceRule(1, productService.findByCode("ult_small"), 30, 3, 3, 1, false));
        priceRules.add(new PriceRule(2, productService.findByCode("ult_large"), 30, 3, 3, 39.90));
        priceRules.add(new PriceRule(3, 1, productService.findByCode("1gb"), productService.findByCode("ult_medium"), -1, 1, 1));
        priceRules.add(new PriceRule(4, "I<3AMAYSIM", 10.00));
    }

    @Test
    public void testTakeOneDiscount() throws AppException{

        ShoppingCart cart = ShoppingCart.nw(priceRules)
                .add(new Item(productService.findByCode("ult_small"), 3))
                .add(new Item(productService.findByCode("ult_large"), 1));

        shoppingCartService.calculate(cart);



        print("testTakeOneDiscount()",cart);
        Assert.assertTrue("Not equal", Utils.toDouble(cart.getTotal(),2) == 94.70);
    }

    @Test
    public void testBulkDiscount() throws AppException{
        ShoppingCart cart = ShoppingCart.nw(priceRules)
                .add(new Item(productService.findByCode("ult_small"), 2))
                .add(new Item(productService.findByCode("ult_large"), 4));

        shoppingCartService.calculate(cart);

        print("testBulkDiscount()",cart);
        Assert.assertTrue("Not equal", Utils.toDouble(cart.getTotal(),2) == 209.40);
    }

    @Test
    public void testBundledProduct() throws AppException{
        ShoppingCart cart = ShoppingCart.nw(priceRules)
                .add(new Item(productService.findByCode("ult_small"), 1))
                .add(new Item(productService.findByCode("ult_medium"), 2));

        shoppingCartService.calculate(cart);

        print("testBundledProduct()",cart);
        Assert.assertTrue("Not equal", Utils.toDouble(cart.getTotal(),2) == 84.70);
    }

    @Test
    public void testPromoCode() throws AppException{
        ShoppingCart cart = ShoppingCart.nw(priceRules)
                .add(new Item(productService.findByCode("ult_small"), 1))
                .add(new Item(productService.findByCode("1gb"), 1), "I<3AMAYSIM");

        shoppingCartService.calculate(cart);

        print("testPromoCode()",cart);
        Assert.assertTrue("Not equal", Utils.toDouble(cart.getTotal(),2) == 31.32);
    }

    private void print(String title, ShoppingCart cart){
        System.out.println("\n"+title);
        cart.getItems().forEach(item->{
            System.out.println(String.format("%d x %s", item.getQuantity(), item.getProduct().getName()));
        });
        System.out.println("Total: " + Utils.toDouble(cart.getTotal(),2));
    }
}
