package com.amaysim.entity;

import com.amaysim.enums.PriceRuleStatus;
import com.amaysim.enums.PriceRuleType;

/**
 * Created by win7 on 1/25/2017.
 */
public class PriceRule extends BaseEntity {

    private Product product;
    private Integer duration;
    private int minItemBought;
    private int maxItemBought;
    private Double discount;
    private PriceRuleStatus status;
    private Product bundledProduct;
    private String promoCode;
    private Integer bundledProductQuantity;
    private Double newPrice;
    private int freeItemQuantity;
    private boolean discountPerItem = false;
    private PriceRuleType type;

    public PriceRule(){}

    public PriceRule(Integer id, Product product,Integer duration, int minItemBought,int maxItemBought, int freeItemQuantity, boolean discountPerItem){
        this.id = id;
        this.product = product;
        this.duration = duration;
        this.minItemBought = minItemBought;
        this.maxItemBought = maxItemBought;
        this.freeItemQuantity = freeItemQuantity;
        this.discountPerItem = discountPerItem;
        this.status = PriceRuleStatus.ENABLED;
        this.type = PriceRuleType.TAKE_ONE;
    }

    public PriceRule(Integer id, Product product, Integer duration, int minItemBought, int maxItemBought, double newPrice){
        this.id = id;
        this.product = product;
        this.duration = duration;
        this.minItemBought = minItemBought;
        this.maxItemBought = maxItemBought;
        this.newPrice = newPrice;
        this.discountPerItem = true;
        this.status = PriceRuleStatus.ENABLED;
        this.type = PriceRuleType.BULK;
    }

    public PriceRule(Integer id, String promoCode, Double discount){
        this.id = id;
        this.promoCode = promoCode;
        this.discount = discount;
        this.status = PriceRuleStatus.ENABLED;
        this.type = PriceRuleType.PROMO_CODE;
    }

    public PriceRule(Integer id, Integer bundledProductQuantity, Product bundledProduct, Product product, Integer duration, int minItemBought, int maxItemBought){
        this.id = id;
        this.bundledProductQuantity = bundledProductQuantity;
        this.bundledProduct = bundledProduct;
        this.product = product;
        this.duration = duration;
        this.minItemBought = minItemBought;
        this.maxItemBought = maxItemBought;
        this.status = PriceRuleStatus.ENABLED;
        this.type = PriceRuleType.BUNDLE;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public int getMinItemBought() {
        return minItemBought;
    }

    public void setMinItemBought(int minItemBought) {
        this.minItemBought = minItemBought;
    }

    public int getMaxItemBought() {
        return maxItemBought;
    }

    public void setMaxItemBought(int maxItemBought) {
        this.maxItemBought = maxItemBought;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public PriceRuleStatus getStatus() {
        return status;
    }

    public void setStatus(PriceRuleStatus status) {
        this.status = status;
    }

    public Product getBundledProduct() {
        return bundledProduct;
    }

    public void setBundledProduct(Product bundledProduct) {
        this.bundledProduct = bundledProduct;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Integer getBundledProductQuantity() {
        return bundledProductQuantity;
    }

    public void setBundledProductQuantity(Integer bundledProductQuantity) {
        this.bundledProductQuantity = bundledProductQuantity;
    }

    public boolean isDiscountPerItem() {
        return discountPerItem;
    }

    public void setDiscountPerItem(boolean discountPerItem) {
        this.discountPerItem = discountPerItem;
    }

    public Double getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(Double newPrice) {
        this.newPrice = newPrice;
    }

    public PriceRuleType getType() {
        return type;
    }

    public void setType(PriceRuleType type) {
        this.type = type;
    }

    public int getFreeItemQuantity() {
        return freeItemQuantity;
    }

    public void setFreeItemQuantity(int freeItemQuantity) {
        this.freeItemQuantity = freeItemQuantity;
    }
}
