package com.amaysim.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by win7 on 1/25/2017.
 */
public class BaseEntity implements Serializable {

    protected Integer id;
    protected Date createdOn;
    protected Date updatedOn;

    public BaseEntity(){}


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
