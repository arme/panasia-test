package com.amaysim.entity;

import java.math.BigDecimal;

/**
 * Created by win7 on 1/25/2017.
 */
public class Item extends BaseEntity {

    private int quantity;
    private Product product;
    private boolean bundled = false;
    private BigDecimal total;

    public Item(Product product, int quantity){
        this.product = product;
        this.quantity = quantity;

        total = new BigDecimal(product.getPrice() * quantity);
    }

    public Item(Product product, int quantity, boolean bundled){
        this.product = product;
        this.quantity = quantity;
        this.bundled = bundled;

        if(bundled){
            total = BigDecimal.ZERO;
        }
        else {
            total = new BigDecimal(product.getPrice() * quantity);
        }
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public boolean isBundled() {
        return bundled;
    }

    public void setBundled(boolean bundled) {
        this.bundled = bundled;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
