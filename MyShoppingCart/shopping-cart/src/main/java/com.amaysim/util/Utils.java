package com.amaysim.util;

import java.math.BigDecimal;

/**
 * Created by win7 on 1/26/2017.
 */
public class Utils {

    public static boolean isEquals(Integer int1, Integer int2){
        return int1.compareTo(int2) == 0;
    }

    public static BigDecimal getDiscountedPrice(BigDecimal price, double discount){
        BigDecimal discountPrice = price.multiply(new BigDecimal(discount/100));
        return price.subtract(discountPrice);
    }

    public static Double toDouble(BigDecimal bd, int scale){
        return bd.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}
