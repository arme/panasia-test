package com.amaysim.exception;

/**
 * Created by win7 on 1/26/2017.
 */
public class AppException extends Exception {

    public AppException(Throwable t){
        super(t);
    }

    public AppException(String message, Throwable t){
        super(message, t);
    }
}
