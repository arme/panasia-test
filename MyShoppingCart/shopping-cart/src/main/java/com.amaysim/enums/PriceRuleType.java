package com.amaysim.enums;

/**
 * Created by win7 on 1/26/2017.
 */
public enum PriceRuleType {
    TAKE_ONE, BULK, BUNDLE, PROMO_CODE
}
