package com.amaysim.enums;

/**
 * Created by win7 on 1/25/2017.
 */
public enum PriceRuleStatus {
    ENABLED, DISABLED
}
