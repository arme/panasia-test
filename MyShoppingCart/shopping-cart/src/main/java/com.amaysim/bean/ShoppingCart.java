package com.amaysim.bean;

import com.amaysim.entity.Item;
import com.amaysim.entity.PriceRule;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by win7 on 1/25/2017.
 */
public class ShoppingCart implements Serializable {

    private List<PriceRule> priceRules;
    private LinkedList<Item> items;
    private BigDecimal total;
    private List<String> promoCodes;

    private ShoppingCart(List<PriceRule> priceRules) {
        this.priceRules = priceRules;
        items = new LinkedList<Item>();
        promoCodes = new ArrayList();
    }

    public static ShoppingCart nw(List<PriceRule> priceRules) {
        return new ShoppingCart(priceRules);
    }

    public ShoppingCart add(Item item) {
        for (Item currentItem : items) {
            if (currentItem.getProduct().getId().compareTo(item.getProduct().getId()) == 0) {
                currentItem.setQuantity(currentItem.getQuantity() + item.getQuantity());
                return this;
            }
        }

        items.add(item);
        return this;
    }

    public ShoppingCart add(Item item, String promoCode) {
        add(item);
        promoCodes.add(promoCode);
        return this;
    }

    public List<PriceRule> getPriceRules() {
        return priceRules;
    }

    public void setPriceRules(List<PriceRule> priceRules) {
        this.priceRules = priceRules;
    }

    public LinkedList<Item> getItems() {
        return items;
    }

    public void setItems(LinkedList<Item> items) {
        this.items = items;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.total = this.total.add(subTotal);
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        return sb.toString();
    }

    public List<String> getPromoCodes() {
        return promoCodes;
    }

    public void setPromoCodes(List<String> promoCodes) {
        this.promoCodes = promoCodes;
    }
}
