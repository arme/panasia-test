package com.amaysim.service;

import com.amaysim.exception.AppException;

import java.util.List;

/**
 * Created by win7 on 1/26/2017.
 */
public interface GenericPersistence<T> {

    void persist(T t) throws AppException;
    void remove(T t) throws AppException;
    T merge(T t) throws AppException;
    T findById(Integer id) throws AppException;
    List<T> findAll() throws AppException;

}
