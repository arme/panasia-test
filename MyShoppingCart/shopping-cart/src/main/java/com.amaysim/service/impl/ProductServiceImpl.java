package com.amaysim.service.impl;

import com.amaysim.entity.Product;
import com.amaysim.exception.AppException;
import com.amaysim.service.ProductService;

import java.util.List;

/**
 * Created by win7 on 1/26/2017.
 */
public class ProductServiceImpl implements ProductService {
    @Override
    public Product findByCode(String code) throws AppException {
        return null;
    }

    @Override
    public void persist(Product product) throws AppException {

    }

    @Override
    public void remove(Product product) throws AppException {

    }

    @Override
    public Product merge(Product product) throws AppException {
        return null;
    }

    @Override
    public Product findById(Integer id) throws AppException {
        return null;
    }

    @Override
    public List<Product> findAll() throws AppException {
        return null;
    }
}
