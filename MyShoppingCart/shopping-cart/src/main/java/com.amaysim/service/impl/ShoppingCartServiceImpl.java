package com.amaysim.service.impl;

import com.amaysim.bean.ShoppingCart;
import com.amaysim.entity.Item;
import com.amaysim.entity.PriceRule;
import com.amaysim.entity.Product;
import com.amaysim.enums.PriceRuleType;
import com.amaysim.service.ShoppingCartService;
import com.amaysim.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by win7 on 1/25/2017.
 */
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Override
    public void calculate(ShoppingCart cart) {

        cart.setTotal(BigDecimal.ZERO);

        applyPriceRules(cart);
        applyPromoCode(cart);
        calculateTotal(cart);
    }

    private void calculateTotal(ShoppingCart cart){
        cart.getItems().forEach(item -> {
            cart.setSubTotal(item.getTotal());
        });
    }

    private void applyPromoCode(ShoppingCart cart) {
        for (String promoCode : cart.getPromoCodes()) {
            cart.getPriceRules().stream()
                    .filter(priceRule -> (priceRule.getType() == PriceRuleType.PROMO_CODE && priceRule.getPromoCode() != null && priceRule.getPromoCode().equals(promoCode)))
                    .forEach(priceRule -> {
                for (Item item : cart.getItems()) {
                    if(item.isBundled()){
                        continue;
                    }
                    item.setTotal(Utils.getDiscountedPrice(item.getTotal(), priceRule.getDiscount()));
                }
            });
        }
    }

    private void applyPriceRules(ShoppingCart cart) {
        List<PriceRule> priceRules = cart.getPriceRules();

        List<Item> bundledItems = new ArrayList<>();

        cart.getItems().forEach(item -> {


            priceRules
                    .stream()
                    .filter(priceRule -> priceRule.getProduct() != null && priceRule.getType() != PriceRuleType.PROMO_CODE)
                    .forEach(
                    priceRule -> {
                        int quantity = item.getQuantity();
                        if (Utils.isEquals(priceRule.getProduct().getId(), item.getProduct().getId())) {
                            switch (priceRule.getType()) {
                                case TAKE_ONE:
                                    if (priceRule.getMinItemBought() == priceRule.getMaxItemBought()
                                            && priceRule.getMinItemBought() >= quantity
                                            && priceRule.getMaxItemBought() <= quantity) {
                                        Product p = item.getProduct();
                                        if (p != null) {
                                            BigDecimal discountPrice = new BigDecimal(quantity/priceRule.getMinItemBought()).multiply(new BigDecimal(p.getPrice()));

                                            item.setTotal(new BigDecimal(p.getPrice() * quantity).subtract(discountPrice));
                                        }
                                    }
                                    break;
                                case BULK:
                                    if (priceRule.getMinItemBought() < quantity
                                            && priceRule.getMaxItemBought() < quantity
                                            && priceRule.getNewPrice() != null) {
                                        Product p = item.getProduct();
                                        if (p != null) {
                                            item.setTotal(new BigDecimal(priceRule.getNewPrice()).multiply(new BigDecimal(quantity)));
                                        }
                                    }
                                    break;
                                case BUNDLE:
                                    if (priceRule.getBundledProductQuantity() != null
                                            && priceRule.getBundledProduct() != null) {
                                        bundledItems.add(new Item(priceRule.getBundledProduct(), priceRule.getBundledProductQuantity()*item.getQuantity(), true));

                                        item.setTotal(new BigDecimal(item.getProduct().getPrice()).multiply(new BigDecimal(quantity)));
                                    }
                                    break;
                                default:
                                    item.setTotal(new BigDecimal(item.getProduct().getPrice()).multiply(new BigDecimal(quantity)));
                                    break;
                            }
                        }
                    }
            );
        });

        cart.getItems().addAll(bundledItems);
    }
}
