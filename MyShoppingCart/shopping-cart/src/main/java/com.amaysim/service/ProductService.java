package com.amaysim.service;

import com.amaysim.entity.Product;
import com.amaysim.exception.AppException;

/**
 * Created by win7 on 1/26/2017.
 */
public interface ProductService extends GenericPersistence<Product>{

    Product findByCode(String code) throws AppException;
}
