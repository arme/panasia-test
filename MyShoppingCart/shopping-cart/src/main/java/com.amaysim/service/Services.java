package com.amaysim.service;

import com.amaysim.service.impl.ProductServiceImpl;
import com.amaysim.service.impl.ShoppingCartServiceImpl;

/**
 * Created by win7 on 1/26/2017.
 */
public class Services {

    public static ProductService getProductService() {
        return new ProductServiceImpl();
    }

    public static ShoppingCartService ShoppingCartService() {
        return new ShoppingCartServiceImpl();
    }
}
