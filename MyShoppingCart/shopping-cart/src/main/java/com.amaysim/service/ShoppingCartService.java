package com.amaysim.service;

import com.amaysim.bean.ShoppingCart;

/**
 * Created by win7 on 1/25/2017.
 */
public interface ShoppingCartService {

    void calculate(ShoppingCart cart);
}